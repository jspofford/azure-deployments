#!/bin/zsh
distro=$(cat /etc/*release | grep debian)
scripts_dir="/workspace/.devcontainer/.scripts"
container_env_configs_dir="${scripts_dir}/container-env-configs"

# Copy/create/append files from files-to-copy directory, over to the container
for file in $(ls -A "${container_env_configs_dir}/.")
do
  echo "Copying conents of ${file} to ${HOME}/${file}"
  cat "${container_env_configs_dir}/${file}" >> "${HOME}/${file}"
done

echo "--- Updating distro ---"
[[ $distro ]] && distro_cmd="apt-get" || distro_cmd="yum"   # if debian, set command to apt, else it's redhat/centos so set to yum
$distro_cmd  update -y

if [[ -s "${scripts_dir}/pkglist" ]]; then
  echo "--- Installing user defined packages ---"
  $distro_cmd install -y $(cat "${scripts_dir}/pkglist") 
fi

# Remove docker-compose version 1 if it exists
docker_compose_version_2=$(docker compose version | grep "Docker Compose version v2")
if [[ -n $docker_compose_version_2 ]]; then
  if [[ -f /usr/local/bin/docker-compose ]]; then
    echo "--- Docker Compose has been upgraded to ${docker_compose_version} --- Removing version 1 of docker-compose ---"
    rm /usr/local/bin/docker-compose  
  fi
fi

# Install azure-cli
if [[ $distro ]]; then
  echo "--- Installing azure-cli ---"
  curl -sL https://aka.ms/InstallAzureCLIDeb | bash
fi

if [[ -f "${scripts_dir}/ansible-galaxy-collections" ]]; then
  echo "--- Installing user ansible-galaxy collections ---"
  ansible-galaxy collection install $(cat "${scripts_dir}/ansible-galaxy-collections")
fi

echo "--- Installing python modules ---"
pip install -r /workspace/deploy/requirements.txt

echo 
echo "You can find the logs of this post-create command in /workspace/post-create.log"
echo