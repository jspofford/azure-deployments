# README

## ABOUT

The files in this directory can be used to customize the `.devcontainer` on container creation.

### About the files

- __files-to-copy__ - A list of configuration files to copy over and use to configure your ZSH/Linux environment.
> Examples: 
> - __[.inputrc](https://ss64.com/bash/syntax-inputrc.html)__ - Useful for customizing the way Tab-completion works, e.g. with the `ls` command.
> - __[.zshrc](https://ohmyz.sh/)__ - oh-my-zsh is installed on the devcontainer, and the default shell is `zsh`.  You can use this file to appended specific settings that you want in your `.zshrc` file without worrying about overwritting your `zshrc` configuration.  The `zshrc` file sets the environment for the interactive shell being used.

- __[post-create](https://containers.dev/implementors/json_reference/#formatting-string-vs-array-properties:~:text=vs%20array%20properties.-,postCreateCommand,-string%2C%0Aarray)__ - This file is called from the `devcontainer.json's` `postCreateCommand`.  After the container is created, this command will run (it will not run on every startup, only upon container creation).  The current objective of this is to call the `post-create` script which will copy any configuration files and install any packages that you'd like automatically installed on the container.  Packages you want installed should be added to the `pkglist` file in this directory.

- __pkglist__ - You may add linux packages to install by added them to the pkglist file.  Just add the name of each one on a separate line, like the example below.

```bash
vim
azure-cli
```

NOTE: The post-create script will run only once when the container is created.  It will not run again when the container is restarted.
